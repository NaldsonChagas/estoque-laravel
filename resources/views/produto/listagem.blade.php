@extends('layout.principal')
@section('conteudo')

    <table class="table">
        <thead>
        <tr>
            <th>Nome:</th>
            <th>Descrição:</th>
            <th>Valor:</th>
            <th>Quantidade:</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $produtos as $p )

            <tr class="{{ $p->quantidade <= 1 ? 'danger' : ''}}">
                <td>{{ $p->nome }}</td>
                <td>{{ $p->descricao }}</td>
                <td>{{ $p->valor }}</td>
                <td>{{ $p->quantidade }}</td>
                <td>
                    <a href="{{action('ProdutoController@detalhes', $p->id)}}">
                        Visualizar
                    </a>
                </td>
                <td>
                    <a href="{{action('ProdutoController@remove', $p->id)}}">
                        Deletar
                    </a>
                </td>
            </tr>

        @endforeach

        </tbody>
    </table>
    @if(old('nome'))
        <div class="alert alert-success">O produto {{old('nome')}} foi adicionado com sucesso!</div>
    @endif

@stop
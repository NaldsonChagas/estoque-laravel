@extends('layout.principal')
@section('conteudo')


    <h2 class="h2 text-left">Adicione um novo produto</h2>

    <form method="post" action="/produtos/adiciona">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Nome" name="nome">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" placeholder="Valor" name="valor">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" placeholder="Quantidade" name="quantidade">
        </div>

        <div class="form-group">
            <input type="text" class="form-control" placeholder="Descrição" name="descricao">
        </div>
        <input type="hidden" value="{{ csrf_token() }}" name="_token">
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
    </form>

@stop

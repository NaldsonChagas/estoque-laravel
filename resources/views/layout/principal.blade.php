<html>
<head>
    <link href="/css/app.css" rel="stylesheet" type="text/css">
    <link href="/css/custom.css" rel="stylesheet" type="text/css">
    <title>Controle de estoque</title>
</head>
<body>
<div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">

            <div class="navbar-header">
                <a class="navbar-brand" href="/">Estoque Laravel</a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{action('ProdutoController@lista')}}">Listagem</a></li>
                <li><a href="{{action('ProdutoController@novo')}}">Novo</a></li>
            </ul>

        </div>
    </nav>

    <div class="container">
        @yield('conteudo')
    </div>

    <footer class="footer">
        <p>© Curso de Laravel do Alura.</p>
        <div class="pull-right">
            <a href="{{action('ProdutoController@toJson')}}">API</a>
        </div>
    </footer>

</div>
</body>
</html>
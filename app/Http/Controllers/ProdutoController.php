<?php
namespace App\Http\Controllers;

use Request;
use App\Produto;

class ProdutoController extends Controller
{
	public function lista()
	{
		$produtos = Produto::all();
		return view("produto.listagem")->with("produtos", $produtos);
	}
	public function detalhes($id)
	{
		$produto = Produto::find($id);
		if (empty ($produto))
			return redirect()->action('ProdutoController@lista');
		return view("produto.detalhes")->with('produto', $produto);
	}
	public function novo()
	{
		return view('produto.novo');
	}
	public function adiciona()
	{
		Produto::create(Request::all());
		return redirect()->action('ProdutoController@lista')->withInput(Request::only('nome'));
	}
	public function remove($id)
	{
		$produto = Produto::find($id);
		$produto->delete();
		return redirect()->action('ProdutoController@lista');
	}
	public function toJson()
	{
		$produtos = Produto::all();
		return response()->json($produtos);
	}
}
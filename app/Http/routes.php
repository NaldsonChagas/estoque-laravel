<?php
Route::get('/', 'ProdutoController@lista');
Route::get('/produtos/detalhes/{id}', 'ProdutoController@detalhes');
Route::get('/produtos/novo', 'ProdutoController@novo');
Route::post('/produtos/adiciona', 'ProdutoController@adiciona');
Route::get('/produtos/remove/{id}', 'ProdutoController@remove');
Route::get('/api/produtos/json', 'ProdutoController@toJson');
